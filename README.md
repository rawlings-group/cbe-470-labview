# CBE 470 Labview Files #

This repo contains all of the Labview VIs necessary for labs in CBE 470.
Files are mostly organized by experiment, but VIs used in multiple
experiments (e.g., `AcquireData.vi`) are stored in the `Common` folder.

The same "Front Panel" VIs are used for both physical and simulation
versions of experiments. In some cases you will have to choose which
version you are performing on the panel, so be aware of this.

## Installation and Dependencies ##

To use all of these files, you will need Labview 2015 (or newer), as
well as Matlab R2012b (or newer). For these VIs, there is no specific
installation process, other than unzipping the folder to a convenient location.
All of the necessary VIs are contained within subfolders of this repo.

Note that if your Labview version is newer than 2015, you may be
prompted to save any VIs that you open (or that are opened by any
VIs you open). This is okay, and you should choose "Save All" to
avoid being prompted in the future. Saving will render these VIs
incompatible with older version of Labview, but you can always
download a fresh copy of this repo from Bitbucket if you need it.

## Notes ##

* For any VIs that use Matlab (the simuation VIs), when you start Labview, a
  "Matlab Command Window" will pop up. DO NOT close this window, or you will
  have to quit your current test and restart Labview.

* The VIs that are used by multiple experiments are included in the `Common`
  folder, i.e., `AcquireData.vi` (AKA `Scott's Acquire Data.vi` for
  general data acquisition) and `StirredTankSimulation.vi` (used as the
  apparatus for Experiments 6 and 8).

* The Experiment 9 simulation requires a data file (`distillationpars.mat`).
  To run. By default, it should be present in the zip download, but it can be
  produced by running `makedistillationpars.m` in Matlab (which is not included
  in the zip, as it contains the "answers" for the model parameters).

* The Experiment 9 simulation (specifically the Matlab interface) has proven to
  be a bit finicky. On the VI front panel, you will notice that that there are
  two "Matlab Status" boxes. If any errors ever appear in these boxes, you will
  need to restart Labview. In particular, closing the "Matlab Command Window"
  will cause an error.

* All of the VIs in the `Library` folder are small simple VIs used by the main
  VIs. You should never need to open any of these directly. Note that these
  files used to be stored in a folder `user.lib`, typically in
  ```
  C:\Program Files\National Instruments\LabVIEW 20XX\user.lib
  ```
  If you see any messages about some files being found inside this folder, you
  can typically ignore it. If you do get any Labview errors related to these
  VIs, delete the corresponding VIs from the `user.lib` folder and see if that
  fixes the issues.

## Distribution ##

The intended distribution method is via a zip file download from
[Bitbucket][dllink]. As updates are made to the VI files, be sure
to update the zip file in the Bitbucket downloads section. Note that
you can easily build the zip file using `make dist`. To actually upload the zip
file, use `make upload`. Note that you will be asked for your Bitbucket username
and password. Alternatively, you can use Bitbucket's web interface for upload.

If, for any reason, you need a complete copy of the files or if you think
the zip file is out of date, you can also download the entire zip file
through the [Bitbucket][dllink] link. Choose "Download repository".

[dllink]: https://bitbucket.org/rawlings-group/cbe-470-labview/downloads
