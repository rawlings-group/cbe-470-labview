#!/usr/bin/env python3
"""Adds all VIs to a zip file for upload to Bitbucket."""

import argparse
import os
import sys
import zipfile

# Command-line arguments.
parser = argparse.ArgumentParser(description=__doc__, add_help=False)
parser.add_argument("--help", help="print this help", action="help")
parser.add_argument("--include-user-lib", action="store_true",
                    help="add files in user.lib to zip")
group = parser.add_mutually_exclusive_group()
group.add_argument("--root-folder", help="name for root folder in zip file")
group.add_argument("--no-root-folder", action="store_true",
                   help="don't include root folder in zip file")
parser.add_argument("--name", help="specify name for zip file",
                    default="CBE-470-VIs.zip")
parser.add_argument("--explicit", action="store_true",
                    help="Only include explicitly given FILEs")
parser.add_argument("file", default=[], nargs="*",
                    help="Files to include if --explicit is given")
kwargs = vars(parser.parse_args(sys.argv[1:]))

# Decide what files to include.
if kwargs["explicit"]:
    # Just use explicit list of files.
    includefiles = kwargs["file"]
else:
    # Try to infer what the files are.
    print ("***WARNING: any extraneous files may be included in the"
           "distribution. Consider running with --explicit via `make`.")

    # Specify directories.
    directories = [
        "Common",
        "E10-Copoly",
        "E2-TwoTankControl",
        "E3-DataAcquisition",
        "E4-ProcessID",
        "E5-LevelControl",
        "E6-FbFfCascade",
        "E8-StirredTankMV",
        "E9-Distillation",
    ]
    excludefiles = set([
        "E9-Distillation/runsteptests.m",
        "E9-Distillation/makedistillationpars.m",
        "E9-Distillation/distillationsteptest-u1.txt",
        "E9-Distillation/distillationsteptest-u2.txt",
        "E9-Distillation/distillationsteptest-u3.txt",
    ])
    if kwargs["include_user_lib"]:
        directories.append("user.lib")

    # Loop through directories.
    includefiles = []
    for directory in directories:
        for (ddir, _, files) in os.walk(directory):
            for fl in files:
                readfile = os.path.join(ddir, fl)
                if readfile not in excludefiles:
                    includefiles.append(readfile)

# Pick filename and whether to include root directory.
zipname = kwargs["name"]
if kwargs["no_root_folder"]:
    root = ""
else:
    root = kwargs["root_folder"]
    if root is None:
        root = os.path.splitext(os.path.split(zipname)[1])[0]

# Now add files.
with zipfile.ZipFile(zipname, "w", zipfile.ZIP_DEFLATED) as vizip:
    for readfile in includefiles:
        writefile = os.path.join(root, readfile)
        vizip.write(readfile, writefile)

    # Also add readme with txt extension to play nice with Windows.
    vizip.write("README.md", os.path.join(root, "README.txt"))
    print("Wrote zip file '%s'." % vizip.filename)
