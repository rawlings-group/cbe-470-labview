function runsteptests()
% Runs all of the system identifican step tests.

% First, do setup.
makedistillationpars();
[x0, ~, ~, pars] = distillationsetup();
fprintf('Done with initialization.\n')

% Define step tests in terms of delta u.
steptests = diag([-0.06, 0.0336, 8]);

% Pick some sizes.
Nu = 3;
Ny = 3;
Nsteptests = 3;

Nsamples = 1;
controllaw = 'Manual';
d = pars.dss;

Nsim = 190;
Ninit = 10;

% Now run actual tests.
Nt = Nsim + Ninit;
Y = NaN(Nsteptests, Ny, Nt + 1);
U = NaN(Nsteptests, Nu, Nt);
for i = 1:Nsteptests
    x = x0;
    Y(i,:,1) = pars.yss;
    datafile = fopen(sprintf('distillationsteptest-u%d.txt', i), 'w');
    fprintf(datafile, 'time,y1,y2,y3,u1,u2,u3,d1,d2\n');
    for t = 1:Nt
        u = pars.uss;
        if t > Ninit
            u = u + steptests(:,i);
        end
        [y, x, u] = distillationstep(x, u, d, Nsamples, controllaw, pars);
        U(i,:,t) = u;
        Y(i,:,t+1) = y;
        datastr = sprintf('%.6f,', [t; y; u; d]);
        fprintf(datafile, '%s\n', datastr(1:end-1));
    end
    fclose(datafile);
end
fprintf('Done with simulation.\n');

% Make plots.
steptestplot(U, Y);
fprintf('Done with plots.\n');

end%function

function steptestplot(U, Y)
    % Makes a plot of the step tests.
    Nsteptests = size(U, 1);
    Nu = size(U, 2);
    Ny = size(Y, 2);
    if Nu ~= Ny
        error('Nu and Ny must be the same!');
    end
    Nt = size(U, 3);
    
    figure();
    for i = 1:Nsteptests
        for j = 1:Nu
            % Plot data.
            subplot(Nu, Nsteptests, (j - 1)*Nsteptests + i);
            [tu, u, ty, y] = getuy(U, Y, i, j);
            [ax, yline, uline] = plotyy(ty, y, tu, u);
            set(ax, {'ycolor'}, {'b'; 'r'});
            set(yline, 'color', 'b');
            set(uline, 'color', 'r');
            
            % Plot dashed lines for steady-states.
            hold(ax(1), 'on');
            plot(ax(1), [1, Nt], y(1)*[1, 1], ':b')
            hold(ax(2), 'on');
            plot(ax(2), [1, Nt], u(1)*[1, 1], ':r');
            
            % Make labels.
            if j == 1
                title(sprintf('Step in u_%d', i));
            end
            if i == 1
                ylabel(ax(1), sprintf('y_%d', j));
            elseif i == Nsteptests
                ylabel(ax(2), sprintf('u_%d', j));
            end
        end
    end
end%function

function [tu, u, ty, y] = getuy(U, Y, i, j)
    % Returns plot vectors for u and y.
    ty = (0:size(U, 3))';
    y = squeeze(Y(i,j,:));
    [tu, u] = stairs(ty(1:end-1), squeeze(U(i,j,:)));
    tu = [tu; ty(end)];
    u = [u; u(end)];
end