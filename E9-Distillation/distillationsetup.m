function [x, preverr, interr, pars] = distillationsetup()
% [x, preverr, interr, pars] = distillationsetup()
%
% Defines parameters for the distillation column. This function is used
% in both the simulation and the controller VI, so be sure to test any
% changes in both places.
pars = load('distillationpars.mat');
x = zeros(pars.n, 1);
preverr = zeros(size(pars.yss));
interr = preverr;
end
    

