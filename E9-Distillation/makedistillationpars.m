function makedistillationpars()
% makedistillationpars()
%
% Creates distillationpars.mat, which is needed for the distillation
% simulation. Note that this function requires a recent release of the
% control systems toolbox.

% Variable bounds, etc.
pars = { ...
    % Steady-state values...
    'yss', [0.7; 0.52; 92]; % ...for outputs.
    'uss', [0.18; 0.0464; 20]; % ...for inputs.
    'dss', [0.8; 78]; % ...for disturbances.
    
    % Bounds on u and y.
    'ylb', [0.55; 0.37; 80];
    'yub', [0.85; 0.67; 99.9];
    'ulb', [0.045; 0.0001; 5];
    'uub', [0.25; 0.1; 35];
    'dlb', [0.2; 70];
    'dub', [1.2; 90];
    
    % Noise factor.
    'stdev_y', [0.01; 0.01; 0.25];
    
    % Timestep.
    'Delta', 1;
};
pars = pars';
pars = struct(pars{:});

% System parameters. First three columns are manipulated variables. Final
% two columns are disturbances. tau and theta are in seconds.
pars.K = [ ...
       2/3   -25/42   -7/1600   1/10  -1/300   
     -25/63  -50/21  -19/1600  11/20  -1/300   
    -100/3   625/14    7/8      5/2    1/3  
];
pars.tau = [ ...
   25   30   30   10   10
   20   15   25   10   10
   30   40   40   15   10
];
pars.theta = [ ...
   20   30   20   15   10
   40   25   15   10   10
   45   50   15   10    0
];

% Now get system matrices. Build CT transfer functions and convert to DT
% state-space model to use for simulations.
[I, J] = size(pars.K);
G = tf(zeros(I, J));
for i = 1:I
    for j = 1:J
        G(i,j) = tf(pars.K(i,j), [pars.tau(i,j), 1], ...
            'InputDelay', pars.theta(i,j));
    end
end
dsys = absorbDelay(c2d(ss(G), pars.Delta, 'zoh'));
pars.A = dsys.A;
pars.B = dsys.B(:,1:3); % First three "inputs" are manipulated variables.
pars.Bd = dsys.B(:,4:5); % Final two "inputs" are disturbances.
pars.C = dsys.C;
pars.D = dsys.D(:,1:3);
pars.Dd = dsys.D(:,4:5);

% Also compute target finder and LQR controller.
n = size(pars.A, 1);
sstarg = inv([pars.A - eye(n), pars.B; pars.C, pars.D]);
    % Gives [xss; uss] = sstarg*[Bd*d; 0].
xssuss = sstarg(:,1:n)*(-pars.Bd); % Gives [xss; uss] = xssuss*d.
xss = xssuss(1:n,:);
uss = xssuss(n+1:end,:);

pars.Q = pars.C.'*diag((pars.yub - pars.ylb).^-2)*pars.C;
pars.R = diag((pars.uub - pars.ulb).^-2);
pars.Kmpc = -dlqr(pars.A, pars.B, pars.Q, pars.R);
pars.Kmpcd = uss - pars.Kmpc*xss;
pars.n = n;

% Save mat file.
save('distillationpars.mat', '-struct', 'pars');

end%function