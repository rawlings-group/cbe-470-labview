function [y, u, d, interr, preverr, uV] = distillationpid(ysp, yV, dV, ...
    Kc, tauI, tauD, H, interr, preverr, pars)
% Multivariable PID control for distillation column. Inputs with V at end
% are [0, 10] voltages that are converted to actual values.

% Make sure everybody is a column vector.
ysp = ysp(:);
yV = yV(:);
dV = dV(:);

% Convert voltage inputs to positional values.
y = yV.*(pars.yub - pars.ylb)/10 + pars.ylb;
d = dV.*(pars.dub - pars.dlb)/10 + pars.dlb;

% Now calculate control law. Note that we need to transpose all the
% controller matrices due to how they're entered. Then, use decoupler and
% ensure u is within the bounds.
err = ysp(:) - y(:);
u = Kc'*(err + (pars.Delta./tauI)'*interr + ...
         tauD'*(err - preverr)/pars.Delta); % PID.
u = H*u + pars.uss; % Decoupler and SS offset.
u = min(max(u, pars.ulb), pars.uub); % Enforce bounds.

% Convert u to voltage.
uV = 10*(u - pars.ulb)./(pars.uub - pars.ulb);

% Add to integral of error and step preverr.
interr = interr(:) + err;
preverr = err;

end%function