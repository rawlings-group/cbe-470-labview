function [y, x, u, yV, uV, dV] = distillationstep(x, u, d, Nsamples, controllaw, pars)
% [y, x, u, yV, uV] = distillationstep(x, u, d, Nsamples, controllaw, [pars])
%
% Runs one step of the distillation column simulation.

if ~exist('pars', 'var')
    pars = load('distillationpars.mat');
end

% Make sure all inputs are column vectors.
x = x(:);
u = u(:);
d = d(:);

% Convert d to deviation variables and figure out what to do with u.
d = d - pars.dss;
switch controllaw
case 'Manual'
    % Don't need to do anything. Just accept u as given.
case 'External'
    % Rescale from [0, 10] V units to positional units.
    u = u.*(pars.uub - pars.ulb)/10 + pars.ulb;
case 'MPC'
    %  Control calculated later in loop. Just pick a dummy value.
    u = pars.uss;
otherwise
    % They picked something bad.
    warning('Invalid control law.')
    u = pars.uss;
end
u = min(max(u, pars.ulb), pars.uub); % Enforce bounds.
u = u - pars.uss; % Convert to deviation variables.

% Now simulate.
for t = 1:Nsamples
    switch controllaw
    case 'MPC'
        % Use the LQR controller with target finder.
        u = pars.Kmpc*x + pars.Kmpcd*d;
        u = min(max(u, pars.ulb - pars.uss), pars.uub - pars.uss);
    end
    x = pars.A*x + pars.B*u + pars.Bd*d;
end
y = pars.C*x;
y = y + pars.stdev_y.*randn(size(y)); % Measurement noise.

% Convert back to positional variables.
y = y + pars.yss;
u = u + pars.uss;
d = d + pars.dss;

% Also calculate [0, 10] Voltage units.
yV = (y - pars.ylb)./(pars.yub - pars.ylb)*10;
uV = (u - pars.ulb)./(pars.uub - pars.ulb)*10;
dV = (d - pars.dlb)./(pars.dub - pars.dlb)*10;

end%function