function [param, Hist, options, inputs, Vmax, A, x0, dt, init_done] = StirredTank_init()
% Initialization script for stirred tank.
% Parameters
Vmax = 4; % max tank vol [L]
Kout = 0.09; % outflow coeff. [L/(s*sqrt(dm))]
A = pi()*1.44^2/4; % cross sec. area of tank [dm^2]

param = struct(...
    'Cout', Kout/sqrt(A), ... % outflow coeff. [L/(s*sqrt(L))]
    'Vmax', Vmax, ... % max. tank volume
    'V_line', 1.75, ... % vol. of feed line (hose) [L]
    'QinMax', 10/60, ... % max. feed flow rate / valve [L/s]
    'QdinMax', 5/60, ... % max. disturbance flow rat / valve [L/s]
    'stdev_Val', 0.005, ... % Standard dev. of valve signals [-]
    'stdev_V', 0.01, ... % Standard dev. of vol. measurement [L]
    'stdev_T', 0.15 ... % Standard dev. of temp. measurements [deg C]
);

% Initial values
%    [V(0),  T(0), Qin(0), Qdin(0), Tin(0), Tdin(0), Tin_del(0), td_feed2l(0)]
x0 = [0.001, 14,   0,      0,       14,     14,      14,         0];

%Array for time delay
l_array = 3000;
Hist = [linspace(0, 2*param.V_line, l_array)', x0(2)* ones(l_array, 1)];

% Options for solver
options = odeset('NonNegative', true(2, 1)); % Both states nonnegative.

% Misc parameters.
%        [T_hot, T_cold, HW_Valve, CW_Valve, HWd_valve, CWd_valve, delay_on]
inputs = [60,    14,     0,        0,        0,         0,         1];
dt = 0.1;

% Dummy output to make sure this script runs first
init_done = 0;

end%function
