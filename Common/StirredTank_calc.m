function [outputs, x0, Hist] = ...
         StirredTank_calc(inputs, x0, dt, param, Hist, options)
% Add input disturbances to valve signals (equivalent to disturbances on
% flow rates)
valves = 3:6;
noise = tanh(inputs(valves)/0.01).*randn(size(valves));
inputs(valves) = max(inputs(valves) + param.stdev_Val*noise, 0);
inputs(valves) = round(inputs(valves), 3); % Quantize for nicer display.

% Inputs
T_hot = inputs(1);       % Hot water temp. [deg. C]
T_cold = inputs(2);      % Cold water temp. [deg. C]
HW_Valve = inputs(3);    % Hot water feed valve position [-]
CW_Valve = inputs(4);    % Cold water feed valve position [-]
HWd_Valve = inputs(5);   % Hot water disturbance valve position [-]
CWd_Valve = inputs(6);   % Cold water disturbance valve position [-]
delay_on = inputs(7);    % Time delay switched on [-]

% Flowrates
Qin = (HW_Valve + CW_Valve)*param.QinMax; % L/s
Qdin = (HWd_Valve + CWd_Valve)*param.QdinMax; % L/s
Q = [Qin Qdin];

% Temperatures
if Qin > 0        %Line Temp. [deg. C]
    Tin = (HW_Valve*T_hot + CW_Valve*T_cold) / (HW_Valve + CW_Valve);
else
    Tin = x0(5);
end
if Qdin > 0       % Dist. Temp. [deg. C]
    Tdin = (HWd_Valve*T_hot + CWd_Valve*T_cold) / (HWd_Valve + CWd_Valve);
else
    Tdin = x0(6);
end

% Tank inflow temperature
% No time delay
if delay_on == 0
    TinVec(1) = Tin;
    
% With time delay
else
    if Qin > 0
        V_hist = Hist(:,1);
        Tin_hist = Hist(:,2);
        
        % delayed temp.
        Tin_del=interp1q(V_hist, Tin_hist, param.V_line);
        if isnan(Tin_del)      % Interpolation not successfull
            Tin_del = x0(7);   % Value from last time step
        end
        
        % update history vectors
        Hist(:,1) = [0; V_hist(1 : end-1) + Qin*dt];
        Hist(:,2) = [Tin; Tin_hist(1 : end-1)];
    else
        Tin_del = x0(7); % Value from last time step
    end
    
    TinVec(1) = Tin_del;
end

TinVec(2) = Tdin; % There is no disturbance time delay

% Solve DEs
[~,x] = ode45(@(t, x) StirredTank_DE(t, x, Q, TinVec, param), ...
                      [0 dt/2 dt], x0(1:2), options);

% initial values for next step
%       [[V, T],   [Qin,Qdin], Tin, Tdin, Tin_del]
newx0 = [x(end, :),     Q,     Tin, Tdin, TinVec(1)];
newx0 = max(newx0, 0); % All states are nonnegative.
bad = isnan(newx0);
newx0(bad) = x0(bad);
x0 = newx0; % Finally overwrite.

%add measurement noise to outputs
noise  = randn(1, 7);
stdev = [param.stdev_V, param.stdev_T, 0, 0, param.stdev_T*ones(1,3)];
outputs = max(0, x0 + stdev.*noise); % All outputs nonnegative.

end

function dxdt = StirredTank_DE(~, x, Q, TinVec, param)
% Calculation of derivatives of stirred tank
%
% x(1): V
% x(2): T
%
% Q(1): Qin
% Q(2): Qdin
%
% TinVec(1) = Tin
% TinVec(2) = Tdin

dxdt = zeros(2, 1); % column vector

Qout = param.Cout*sqrt(max(x(1), 0));

dxdt(1) = Q(1) + Q(2) - Qout;
dxdt(2) = (Q(1)*(TinVec(1) - x(2)) + Q(2)*(TinVec(2) - x(2)))/max(x(1), eps());

if (x(1) >= param.Vmax) && (dxdt(1) > 0)
   dxdt(1) = 0;
end

end
