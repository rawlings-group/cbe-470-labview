function runsteptests()
% Runs all of the system identifican step tests.

thisdir = fileparts(mfilename('fullpath'));
addpath([thisdir, '/../Common'])

% First, do setup.
fprintf('Done with initialization.\n')

% Define step tests in terms of delta u.
steptests = diag([0.025, 0.025]);
uss = [0.25; 0.25];
dss = [0; 0.2];

% Pick some sizes.
Nsteptests = 2;
Nt = 900;
Ninit = 200;

% Now run actual tests.
Y = cell(Nsteptests, 1);
U = cell(Nsteptests, 1);
for i = 1:Nsteptests
    [U{i}, Y{i}] = runstep(uss, dss, steptests(:,i), Nt, Ninit, ...
                           sprintf('steptest-u%d.txt', i));
end
U = shiftcat(U);
Y = shiftcat(Y);
fprintf('Done with simulation.\n');

% Make plots.
dt = 0.1;
steptestplot(U, Y, dt);
fprintf('Done with plots.\n');

end%function

function [U, Y] = runstep(u, d, du, Nt, Ninit, filename)
% Run a step starting from steady state for (u, d), stepping u by du.

% Run initialization and specify parameters.
[param, Hist, options, ~, Vmax, ~, x0, dt, ~] = StirredTank_init();
Nprint = 5; % Print data every 5 timesteps.

T_hot = 57; % Hot water temp. [deg. C]
T_cold = 14; % Cold water temp. [deg. C]
HW_Valve = u(1); % Hot water feed valve position [-]
CW_Valve = u(2); % Cold water feed valve position [-]
HWd_Valve = d(1); % Hot water disturbance valve position [-]
CWd_Valve = d(2); % Cold water disturbance valve position [-]
delay = false(); % Don't use delay.
inputs = [T_hot; T_cold; HW_Valve; CW_Valve; HWd_Valve; CWd_Valve; delay]; 

% First, integrate to steady state.
ssparam = param; % Need to get rid of noise.
ssparam.stdev_Val = 0;
ssparam.stdev_T = 0;
ssparam.stdev_V = 0;
x = [];
i = 0;
while (i < 10) && (isempty(x) || norm(x(1:2) - x0(1:2)) > 1e-5)
    if ~isempty(x)
        x0 = x;
    end
    [~, x, ~] = StirredTank_calc(inputs, x0, 1000*dt, ssparam, Hist, options);
    i = i + 1;
end
if i == 10
   error('Couldn''t converge to steady state. Unstable system?');
end

% Save indices for various subvectors.
yind = [2, 1]; % Gives T_tank, V_tank.
uind = [3, 4]; % Gives U_HW, U_CW.

% Now, do step.
Y = NaN(2, 2*Nt + Ninit + 1);
U = NaN(2, 2*Nt + Ninit);
Y(:,1) = x(yind);
header = sprintf('%s,', 'time(sec)', 'y1', 'y2', 'y3', 'y4', 'u1', 'u2');
datafile = fopen(filename, 'w');
fprintf(datafile, '%s\n', header(1:end-1));
for t = 1:(2*Nt + Ninit)
    if t == Ninit + 1
        % Step inputs after a bit.
        inputs(uind) = inputs(uind) + du;
    elseif t == Ninit + Nt + 1
        % Step back down after a while.
        inputs(uind) = inputs(uind) - du;
    end
    
    % Simulate process.
    U(:,t) = inputs(uind);
    [outputs, x, Hist] = StirredTank_calc(inputs, x, dt, param, Hist, options);
    Y(:,t+1) = outputs(yind);
    
    % Save data. V suffix is for Volts.
    if mod(t - 1, Nprint) == 0
        TtankV = outputs(2)/10;
        TlineV = outputs(5)/10;
        TdistV = outputs(6)/10;
        VtankV = outputs(1)/Vmax*10;
        UhotV = U(1,t)*10;
        UcoldV = U(2,t)*10;
        dataline = sprintf('%.5f,', (t-1)*dt, TtankV, TdistV, TlineV, VtankV, UhotV, UcoldV);
        fprintf(datafile, '%s\n', dataline(1:end-1));
    end
end
fclose(datafile);

end%function

function steptestplot(U, Y, dt)
    % Makes a plot of the step tests.
    Nsteptests = size(U, 1);
    Nu = size(U, 2);
    Ny = size(Y, 2);
    if Nu ~= Ny
        error('Nu and Ny must be the same!');
    end
    Nt = size(U, 3);
    
    figure();
    for i = 1:Nsteptests
        for j = 1:Nu
            % Plot data.
            subplot(Nu, Nsteptests, (j - 1)*Nsteptests + i);
            [tu, u, ty, y] = getuy(U, Y, i, j, dt);
            [ax, yline, uline] = plotyy(ty, y, tu, u);
            set(ax, {'ycolor'}, {'b'; 'r'});
            set(yline, 'color', 'b');
            set(uline, 'color', 'r');
            
            % Plot dashed lines for steady-states.
            hold(ax(1), 'on');
            plot(ax(1), [1, Nt], y(1)*[1, 1], ':b')
            hold(ax(2), 'on');
            plot(ax(2), [1, Nt], u(1)*[1, 1], ':r');
            
            % Make labels.
            if j == 1
                title(sprintf('Step in u_%d', i));
            elseif j == Nu
                xlabel('Time (s)');
            end
            if i == 1
                ylabel(ax(1), sprintf('y_%d', j));
            elseif i == Nsteptests
                ylabel(ax(2), sprintf('u_%d', j));
            end
        end
    end
end%function

function [tu, u, ty, y] = getuy(U, Y, i, j, dt)
    % Returns plot vectors for u and y.
    ty = dt*(0:size(U, 3))';
    y = squeeze(Y(i,j,:));
    [tu, u] = stairs(ty(1:end-1), squeeze(U(i,j,:)));
    tu = [tu; ty(end)];
    u = [u; u(end)];
end

function X = shiftcat(x)
    % Returns elements of cell array x concatenated after adding a dim.
    for i = 1:length(x)
        n = ndims(x{i});
        x{i} = permute(x{i}, [n + 1, 1:n]);
    end
    X = cat(1, x{:});
end