function [V1, C1, c1, V2, C2, c2, Cf, cf, Ct, color1, color2, colorf, colort] ...
    = runstep(dt,Qin,qin,V10,C10,c10,V20,C20,c20,x1bot,x2bot,NaOH,inject,testlev)
dt = dt/60000;  % Convert from ms to min.

V1max = 925; % mL
V2max = 950; % mL
Qbotmax = 2500; % mL/min
stdev_Qin = 20;  % mL/min
stdev_qin = 0.04;  % mL/min

CDyein = 0.0046; % M; dye concentration
CNaOHmax = 0.003; % M; NaOH concentration
CNaOHin = CNaOHmax*NaOH; % M; NaOH concentration

% For batch reaction in 925 mL tank and C_NaOH = 0.003 M, tau = 15 min.
% The ODE is V*dC/dT = -k*V*C*C_NaOH, which means
%     tau = 1/(k*C_NaOH) => k = 1/(tau*C_NaOH)
% With units of (min*M)^-1.
%
% We use tau = 10.5 min to make things a little faster.
krxn = 1/(10.5*CNaOHmax); % (min*M)^-1

Vinject = 1;  % 1 mL Dye solution is injected
qinject = inject*Vinject/dt; % mL/min flowrate of injection (Vinject injected over the course of 1 timestep).
Ninject = qinject*CDyein/1000; % mol/min dye flowrate of injection.

if Qin > 0
Qin = Qin + stdev_Qin*randn(1);
end
Qin = max([Qin, 0]);
if qin > 0
qin = qin + stdev_qin*randn(1);
end
qin = max([qin, 0]);

Cf = qin*CDyein/max([Qin + qin,eps()]);
cf = Qin*CNaOHin/max([Qin + qin,eps()]);

% ODE is written in terms of moles.
N10 = C10*(V10/1000); % Convert V10 to L.
B10 = c10*(V10/1000); % Convert V10 to L.

N20 = C20*(V20/1000); % Convert V10 to L.
B20 = c20*(V20/1000); % Convert V10 to L.

tspan = [0,.5,1]*dt;
z0 = [V10; N10; B10; V20; N20; B20];

pars = struct('Qin',Qin,'qin',qin,'krxn',krxn,'CDyein',CDyein,'CNaOHin',CNaOHin,...
            'x1bot',x1bot,'x2bot',x2bot,'Qbotmax',Qbotmax,'V1max',V1max,'V2max',V2max,...
            'qinject',qinject,'Ninject',Ninject);

[~,z] = ode45(@(t,z) process(t,z,pars),tspan,z0);

V1 = min(z(3,1),V1max);
C1 = z(3,2)/V1*1000; % Dye concentration (mol/L).
c1 = z(3,3)/V1*1000; % NaOH concentration.
V2 = min(z(3,4),V2max);
C2 = z(3,5)/V2*1000;
c2 = z(3,6)/V2*1000;

cwater = [250,255,255];
cmid = [0,255,0];
xmid = .5;
cdye = [0,25,0];

colors = [cwater; cmid; cdye];
x0 = [0;xmid;1];
torgb = @(x) int32(interp1(x0,colors,x,'linear'));

colorscale = int32([65536, 256, 1]);
getcolor = @(C) dec2hex(sum(torgb((C/CDyein)^.25).*colorscale));

color1 = getcolor(C1);
color2 = getcolor(C2);
colorf = getcolor(Cf);
Ct = CDyein*testlev;
colort = getcolor(Ct); % Color of test solution.
end

function [dz] = process(~,z,pars)
Qin = pars.Qin;
qin = pars.qin;
krxn = pars.krxn;
CDyein = pars.CDyein;
CNaOHin = pars.CNaOHin;
x1bot = pars.x1bot;
x2bot = pars.x2bot;
Qbotmax = pars.Qbotmax;
V1max = pars.V1max;
V2max = pars.V2max;
qinject = pars.qinject;
Ninject = pars.Ninject;

% V is tank volume, N is moles of dye, B is moles of NaOH.
V1 = z(1); N1 = z(2); B1 = z(3); V2 = z(4); N2 = z(5); B2 = z(6);

Q1in = Qin + qin + qinject;

% Bottom outlet valve flows.
Q1bot = x1bot*Qbotmax*sqrt(V1/V1max);
Q2bot = x2bot*Qbotmax*sqrt(V2/V2max);

% Overflow Streams (Overflow for Tank 1 = Inflow for Tank 2)
if V1 <= V1max
  Q1of = 0;
else
  Q1of = max(0, Q1in - Q1bot); % Must be nonnegative.
end

Q2in = Q1of;

if V2 <= V2max
  Q2of = 0;
else
  Q2of = max(0, Q2in - Q2bot); % Must be nonnegative.
end

% Inlet mole flows (i.e. in mol/min)
N1in = (qin/1000)*CDyein + Ninject;
B1in = (Qin/1000)*CNaOHin;
N2in = (Q2in/V1)*N1;
B2in = (Q2in/V1)*B1;

% d/dt Terms
dV1 = Q1in - Q1bot - Q1of;
dN1 = N1in - ((Q1bot + Q1of)/V1)*N1 - krxn*N1*B1/(V1/1000);
dB1 = B1in - ((Q1bot + Q1of)/V1)*B1 - krxn*N1*B1/(V1/1000);

dV2 = Q2in - Q2bot - Q2of;
dN2 = N2in - ((Q2bot + Q2of)/V2)*N2 - krxn*N2*B2/(V2/1000);
dB2 = B2in - ((Q2bot + Q2of)/V2)*B2 - krxn*N2*B2/(V2/1000);

dz = [dV1; dN1; dB1; dV2; dN2; dB2];
end