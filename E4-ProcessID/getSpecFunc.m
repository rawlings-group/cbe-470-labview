function f = getSpecFunc(alpha,beta,makeplot,nvals)

f = @(C,n) (-.5*(expm1(-alpha*C) + expm1(-beta*C))).^n; 

if nargin() >= 3 && makeplot
    % Data values from sample report.
    data = [0,0; .1,.06; .2,.1; .3,.14; .4,.18; .5,.25; .6,.27; .7,.29; .8,.31; .9,.33; 1,.35];

    Cdata = data(:,1)*1e-5;
    absdata = data(:,2);
    
    figure()
    hold('on')
    plot(Cdata,absdata,'ok')
    fplot(f,Cdata([1,end]).*[1;5],'-k')
end

if nargin() >= 4 && ~isempty(nvals)
    x = linspace(0,1e-5,100)';
    gamma = zeros(size(nvals));
    fapprox = cell(size(nvals));
    figure()
    for i = 1:length(nvals)
        y = f(x,nvals(i));
        yhat = -log(1 - y);
        gamma(i) = x\yhat;
        fapprox{i} = @(C) -expm1(-gamma(i)*C);
        
        yapprox = fapprox{i}(x);
        rmserr = sqrt(mean((y - yapprox).^2));
        
        % Plots.
        subplot(1,length(nvals),i)
        plot(x,y,'-k',x,yapprox,'--k')
        xlabel('Concentration (M)')
        ylabel('Absorbance (-)')
        title(sprintf('n = %.1f RMS Error: %.5g',nvals(i),rmserr));
    end
    
    f = struct('act',f,'approx',fapprox,'gamma',gamma,'n',nvals);
end

end