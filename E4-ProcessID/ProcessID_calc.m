function [t, V1, C1, c1, V2, C2, c2, color1, color2, Cf, cf, colorf, ...
    signalf, signal1, signal2, colort, signalt] = ProcessID_calc( ...
    dt, t, Qin, qin, V10, C10, c10, V20, C20, c20, xbot, options, vfrac, ...
    tosignal, dataFile)
% Calculation for Process ID simulation. Yes, there are a billion inputs
% and outputs, but we have to do this to avoid Labview's Error 1033.
t = t + dt/1000; % Update time in s.

% Constants for bitand.
INJECTION_BIT = 1;
NAOH_BIT = 2;
SAVE_DATA_BIT = 4;

% Enforce minimum tank volume.
V10 = max(1,V10);
V20 = max(1,V20);

[V1, C1, c1, V2, C2, c2, Cf, cf, Ct, color1, color2, colorf, colort] = ...
    runstep(dt, Qin, qin, V10, C10, c10, V20, C20, c20, xbot(1), ...
            xbot(2), bitand(options, INJECTION_BIT), ...
            bitand(options, NAOH_BIT), vfrac);

signal1 = tosignal(C1, 1); % Probe 1
signal2 = tosignal(C2, .9); % Probe 2
signalf = tosignal(Cf, .95); % Feed probe.
signalt = tosignal(Ct, [1,.9]); % Probes 1 and 2 using test concentration.

% Write data if necessary.
if bitand(options, SAVE_DATA_BIT)
  outfile = fopen(dataFile,'a');
  fprintf(outfile,'%f,%f,%f,%f\r\n', t, signal1, signal2, signalf);
  fclose(outfile);
end
end%function