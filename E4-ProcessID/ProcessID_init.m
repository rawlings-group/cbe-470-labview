function [V10, C10, c10, V20, C20, c20, tosignal, done] = ...
    ProcessID_init(V10, C10, c10, V20, C20, c20, dataFile)
% Setup for ProcessID simulation.

% Print header to data file.
outfile = fopen(dataFile,'w');
fprintf(outfile, 'time(s),signal1,signal2,signalf\r\n');
fclose(outfile);

% Initial conditions. Need to convert units.
V10 = max(1, V10); % mL
C10 = C10*1e-6; % mol dye/L
c10 = c10*1e-3; % mol NaOH/L

V20 = max(1, V20); % mL
C20 = C20*1e-6; % mol dye/L
c20 = c20*1e-3; % mol NaOH/L

% other parameters are specified in ProcessID_calc.m

% Parameters for spec function.
alpha = 3e3;
beta = 1.2e5;
tosignal = @(C,n) (-.5*(expm1(-alpha*C) + expm1(-beta*C))).^n; 

done = 1;
end%function

