function [pars, Twater, Tbeaker, Tthermocouple, stdevQ, return_value] = ...
    Thermocouple_init()
% Initialization for thermocouple simulation.

% Approximate Heat Transfer Coefficients (converted to W/cm^2*K)
fix_units = .0001;
U_air_water = 100*fix_units;
U_water_water = 750*fix_units;
U_air_metal = 500*fix_units;
U_water_metal = 2500*fix_units;

% Areas in cm^2.
A_thermocouple = pi()*.2*4;
A_beaker = pi()*5*8;
A_water = pi()*10^2*20;

% Define parameters.
k0 = zeros(3);
k0(1,2) = U_water_water*A_beaker; % Water/Beaker
k0(1,3) = U_water_metal*A_thermocouple; % Water/Thermocouple
k0(2,3) = U_water_metal*A_thermocouple; % Beaker/Thermocouple
k0 = k0 + k0'; % Make symmetric

kair0 = [U_air_water*A_water; ...
              U_air_water*A_beaker; ...
              U_air_metal*A_thermocouple];

% Approximate values in J/K.
rhoVcp = [5440; 420; 0.45*8*pi()*.1^2*4];

pars = struct( ...
	   'thinMul_k',.5, ...
     'thinMul_m',.1, ...
    'beaker_air_fraction',.25, ...
    'k0',k0, ...
    'kair0',kair0, ...
    'rhoVcp',rhoVcp ...
);

Twater = 75;
Tbeaker = 25;
Tthermocouple = 25;

stdevQ = [100; 5; 1]; % Heat noise st. devs.

% Done. Dummy output.
return_value = 0;

end%function