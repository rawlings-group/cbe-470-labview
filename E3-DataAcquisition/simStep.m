function Tout = simStep(Tin, pars)
% T holds the following three temperatures:
%
%   [Twater; Tbeaker; Tthermocouple];
%
% pars contains the following fields:
%
% pars has the following fields
% - Tair (*C)
% - thermocouple_position (0 for in air, 1 for in beaker, 2 for in water)
% - thermocouple_size (0 for thin, 1 for thick)
% - beaker_position (0 for in air, 1 for in water)
% - beaker_air_fraction (fraction of beaker area exposed to air when in water).
% - k0 (3 by 3, W/K)
% - kair0 (3 by 1, W/K)
% - rhoVcp (3 by 1 vector, J/K)
% - Q (3 by 1 vector, W)
% - dt (s)
% - thinMul_k (unitless, ratio of k_thin/k_thick.
% - thinMul_m
%
% For all the constants, element 1 is water properties, 2 is beaker, and
% 3 is thermocouple.

% Define Constants
WATER = 1;
BEAKER = 2;
THERMOCOUPLE = 3;

k = pars.k0;
kair = pars.kair0;

switch pars.thermocouple_size
case 0 % Thin.
	k(:,THERMOCOUPLE) = pars.thinMul_k*k(:,THERMOCOUPLE);
	kair(THERMOCOUPLE) = pars.thinMul_k*kair(THERMOCOUPLE);
	pars.rhoVcp(THERMOCOUPLE) = pars.thinMul_m*pars.rhoVcp(THERMOCOUPLE);
case 1 % Thick.
	% Don't need to do anything.
otherwise
	error('Invalid thermocouple size.')
end

switch pars.beaker_position
case 0 % In air.
    k(WATER,BEAKER) = 0; % No water/beaker coupling.
case 1 % In Water
    k(WATER,BEAKER) = (1 - pars.beaker_air_fraction)*k(WATER,BEAKER); % Adjust k based on exposed area.
    kair(BEAKER) = pars.beaker_air_fraction*kair(BEAKER);
otherwise
    error('Invalid beaker_position.')
end
k(BEAKER,WATER) = k(WATER,BEAKER);

switch pars.thermocouple_position
case 0 % Air.
    k(WATER,THERMOCOUPLE) = 0; % No coupling to water.
    k(BEAKER,THERMOCOUPLE) = 0; % No coupling to beaker.
case 1 % Beaker.
    k(WATER,THERMOCOUPLE) = 0; % No coupling to water.
    kair(THERMOCOUPLE) = 0; % No coupling to air.
case 2 % Water.
    k(BEAKER,THERMOCOUPLE) = 0; % No coupling to beaker.
    kair(THERMOCOUPLE) = 0; % No coupling to air.
otherwise
    error('Invalid thermocouple_position')
end
k(THERMOCOUPLE,WATER) = k(WATER,THERMOCOUPLE);
k(THERMOCOUPLE,BEAKER) = k(BEAKER,THERMOCOUPLE);

odepars = struct('k',k,'kair',kair,'Tair',pars.Tair,'Q',pars.Q, ...
    'rhoVcp',pars.rhoVcp,'Tmax',100,'Tmin',0);
[t, Tout] = ode23(@(t,T) energybal(T,odepars),[0,.5,1]*pars.dt,Tin);

Tout = Tout(end,:)'; % Extract final row.

end

function dT = energybal(T,pars)
% T holds the following three temperatures:
%
%   [Twater; Tbeaker; Tthermocouple];
%
% pars contains the following fields:
% - Tair (*C)
% - kair (3 by 3 vector, W/k)
% - k (3 by 3 matrix, W/K)
% - rhoVcp (3 by 1 vector, J/K)
% - Q (3 by 1 vector, W)
% - Tmin (*C)
% - Tmax (*C)

T = max(min(T,pars.Tmax),pars.Tmin);

dT = (sum(-pars.k.*bsxfun(@minus, T, T'),2) ...
    - pars.kair.*(T - pars.Tair) + pars.Q)./pars.rhoVcp;

end
