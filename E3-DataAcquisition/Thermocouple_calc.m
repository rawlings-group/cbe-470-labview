function [Twater, Tbeaker, Tthermocouple, Twaterss, Tbeakerss] = ...
    Thermocouple_calc(dt, Twater, Tbeaker, Tthermocouple, ...
    thermocouple_position, beaker_position, thermocouple_size, ...
    Qwater, Qbeaker, Tair, stdevQ, pars)
% Simulation step for Thermocouple simulation.
pars.Q = [Qwater; Qbeaker; 0]  ...
                           + randn(3,1).*stdevQ;
pars.thermocouple_position = thermocouple_position;
pars.beaker_position = beaker_position;
pars.thermocouple_size = thermocouple_size;

pars.dt = dt;

pars.Tair = Tair;

Tin = [Twater; Tbeaker; Tthermocouple];

Tout = simStep(Tin, pars);

% Enforce temperature bounds.
Tout = max(0, min(Tout, 100));

Twater = Tout(1);
Tbeaker = Tout(2);
Tthermocouple = Tout(3);

% Calculate SS Temps for beaker/water in air.
Tss = pars.Q./pars.kair0 + pars.Tair;

Twaterss = Tss(1);
Tbeakerss = Tss(2);
end%function