function [t, tmarker, u, umarker, y, ymarker, fcutoff] = oscilloscopeStep( ...
    phi, num_t, tmul, num_u, umul, num_y, ymul, ...
    noise, switches, waveshape, f, A)
% Simulates oscilloscope output for the given settings.
t = (-4*num_t:.01:num_t)*tmul;

omega = 2*pi()*f/1000; % [ms^-1] 

switch waveshape
case 0
     u = A*sin(omega*t + phi);
case 1
     u = A*square(omega*t + phi);
case 2
     u = A*sawtooth(omega*t + pi()/2 + phi,.5);
otherwise
     error('Bad input.')
end
u = u + noise*A*randn(size(u));

switches = logical(switches); % Make sure these are boolean.

% Cutoff frequencies in Hz.
fcutoffs = [.01, 25, 50, 120, 250, 500, 1000, 2000, 3200];
fcutoff = fcutoffs(1) + sum(diff(fcutoffs).*~switches);
om = 2*pi()*fcutoff/1000; % [ms^-1]

% Use a 6th order exponential filter.
Nfilter = 6;
sys = tf(1, [om, 1])^Nfilter;

y = lsim(sys,u,t);
y = y'; % Idk why, but labview makes us do this.

goodinds = (t >= 0);
t = t(goodinds) - num_t*tmul/2;
u = u(goodinds);
y = y(goodinds);

tmarker = (0:num_t)*tmul - num_t*tmul/2;
ymarker = (-num_y:num_y)*ymul;
umarker = (-num_u:num_u)*umul;

end%function