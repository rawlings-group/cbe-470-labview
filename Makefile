# Makefile for the zip distribution.

# List source files here.
COMMON := AcquireData.vi AcquireDataGlobals.vi StirredTank_calc.m \
          StirredTankGlobals.vi StirredTank_init.m StirredTankSimulation.vi

EXP2 := lindata.txt sqrtdata.txt TwoTankControl_calc.m \
        TwoTankControlGlobals.vi TwoTankControl_init.m TwoTankControlPanel.vi \
        TwoTankControlSimulation.vi

EXP3 := OscilloscopeSimulation.vi oscilloscopeStep.m simStep.m \
        Thermocouple_calc.m Thermocouple_init.m ThermocoupleSimulation.vi

EXP4 := getSpecFunc.m ProcessID_calc.m ProcessID_init.m ProcessIDSimulation.vi \
        runstep.m

EXP5 := Standpipe_calc.m StandpipeGlobals.vi Standpipe_init.m \
        StandpipePanel.vi StandpipeSimulation.vi

EXP6 := FbFfCascadePanel.vi

EXP8 := StirredTankMVPanel.vi

EXP9 := DistillationGlobals.vi DistillationPanel.vi distillationpars.mat \
        distillationpid.m distillationsetup.m DistillationSimulation.vi \
        distillationstep.m

EXP10_MATLAB := controller.m copoly.m copolymodel.m copolysample.m \
                PIDController.m
EXP10_LABVIEW := myArrayInput.vi myPlotModule.vi myPulseGenerator.vi myVtoX.vi \
                 myXtoV.vi
EXP10 := copolycontrol.vi copolyprocess.vi tankexplode.png tank.png \
         $(addprefix matlab/, $(EXP10_MATLAB)) \
         $(addprefix myVIs/, $(EXP10_LABVIEW))

LIB := AnalogRead.vi AnalogWrite.vi FirstOrderResponse.vi PID2Variable.vi \
       PositionPID.vi VelocityPI.vi WeedAnaIn.vi WeedAnaOut.vi WeedDIOBtn.vi \
       WeedDIOHi.vi WeedDIOLo.vi WeedDIOTypmatic.vi WeedGenericCommand.vi \
       WeedRelayClose.vi WeedRelayOpen.vi

CBE_470_FILES := $(addprefix Common/, $(COMMON)) \
                 $(addprefix E2-TwoTankControl/, $(EXP2)) \
                 $(addprefix E3-DataAcquisition/, $(EXP3)) \
                 $(addprefix E4-ProcessID/, $(EXP4)) \
                 $(addprefix E5-LevelControl/, $(EXP5)) \
                 $(addprefix E6-FbFfCascade/, $(EXP6)) \
                 $(addprefix E8-StirredTankMV/, $(EXP8)) \
                 $(addprefix E9-Distillation/, $(EXP9)) \
                 $(addprefix E10-Copoly/, $(EXP10)) \
                 $(addprefix Library/, $(LIB))

ZIPNAME := CBE-470-VIs.zip

# Define zip rule.
$(ZIPNAME) : $(CBE_470_FILES) README.md
	@echo "Building zip distribution."
	@./makezip.py --name $(ZIPNAME) --explicit $(CBE_470_FILES) || rm -f $(ZIPNAME)

dist : $(ZIPNAME)
.PHONY : dist

UPLOAD_COMMAND := POST https://api.bitbucket.org/2.0/repositories/rawlings-group/cbe-470-labview/downloads
upload : $(ZIPNAME)
	@echo -n "Enter bitbucket username: " && read bitbucketuser && curl -v -u $$bitbucketuser -X $(UPLOAD_COMMAND) -F files=@"$<"
.PHONY : upload

# Define rules for intermediate files.
MATLAB := matlab -nodisplay

E9-Distillation/distillationpars.mat : E9-Distillation/makedistillationpars.m
	@echo "Building $@."
	@cd E9-Distillation && $(MATLAB) -r "makedistillationpars(); quit()"
	@echo "Done."

# Define cleanup rules.
clean :
	@rm -f $(ZIPNAME) "E9-Distillation/distillationsteptest-u*.txt"
.PHONY : clean

realclean : clean
	@rm -f "E9-Distillation/distillationpars.mat"
.PHONY : realclean

