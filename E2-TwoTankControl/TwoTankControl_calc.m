function [h0, t1Overflow, h1Out, h1Norm, t2Overflow, h2Out, h2Norm, ...
          qRot, qBall, q1, q2] ...
    = TwoTankControl_calc(h0, valveIn, dt, param, options)
% Calculation for two-tank control simulation.

% Grab parameters from struct.
hMax = param.hMax;
qRotMax = param.qRotMax;
qBallMax = param.qBallMax;
q1Max = param.qMax*(h0(1) > 0);
q2Max = param.qMax*(h0(2) > 0);
stdev_q = param.stdev_q;
stdev_h = param.stdev_h;

% Do calculations.
qRot = noise(qRotMax*valveIn(1), stdev_q);
qBall = noise(qBallMax*valveIn(2), stdev_q);

q = zeros(1, 3);
q(1) = qRot + qBall; % Inflow Tank 1.
q(2) = noise(q1Max*valveIn(3), stdev_q); % Outflow Tank 1.
q(3) = noise(q2Max*valveIn(4), stdev_q); % Outflow Tank 2.                                               % Inflow tank 1

% Integration of Diff. Equs.
[~, h] = ode45(@(t,h) VolBal(t, h, q, param), [0, dt/2, dt], h0, options);
h0 = min([h(end,:); hMax*ones(1,2)]); % Initial cond. for next step,
                                    % limited in case of overflow
% Outputs
tOverflow = h(end,:) - hMax;
t1Overflow = tOverflow(1);
t2Overflow = tOverflow(2);
h1 = noise(h0(1), stdev_h);
h2 = noise(h0(2), stdev_h);

CM_TO_IN = 1/2.54;
h1Out = h1*CM_TO_IN; % in inches
h2Out = h2*CM_TO_IN; % in inches
h1Norm = h1/hMax;
h2Norm = h2/hMax;

CM3S_TO_GALMIN = 1/63.08;
qBall = qBall*CM3S_TO_GALMIN; % in gal/min
qRot = qRot*CM3S_TO_GALMIN; % in gal/min
q1 = q(2)*CM3S_TO_GALMIN; % in gal/min
q2 = q(3)*CM3S_TO_GALMIN; % in gal/min
end%function

function dhdt = VolBal(~, ~, q, param)
% Calculation of derivatives of levels h for two tank model
%   
%    q = [q0, q1, q2]
%    param = struct(A1, A2)
dhdt = zeros(2, 1); % Need column vector
dhdt(1) = (q(1) - q(2))/param.A1;
dhdt(2) = (q(2) - q(3))/param.A2;
end%function

function q = noise(q, sigma)
% q = noise(q, sigma)
%
% Corrupts q with normally distributed noise if flow is nonzero, clipping
% negative values to zero.
if q > 0
    q = q + sigma*randn();
end
q = max(q, 0);
end%function