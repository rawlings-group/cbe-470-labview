function [param, options, t0, h0] = TwoTankControl_init()
% Parameters for two-tank control.
d1 = 12; % diameter of tank 1, cm
d2 = d1*sqrt(2); % diameter of tank 2, cm

param = struct();
param.hMax = 30; % tank height, cm
param.qRotMax = 126; % max. rotary valve vlow, cm^3/s
param.qBallMax = 126; % max. ball valve flow, cm^3/2
param.qMax = 180; % max. discharge flow rate, cm^3/s
param.stdev_q = 0.05; % standard dev. for noise of outflow rates, cm^3/s
param.stdev_h = 0.05; % standard dev. for level measurements, cm

param.A1 = pi*d1^2/4;
param.A2 = pi*d2^2/4;

h0 = [0, 0]; % initial conditions for integration
options = odeset(); % No special options.

t0 = 0; % Dummy output to make sure this script finishes first.
end%function
