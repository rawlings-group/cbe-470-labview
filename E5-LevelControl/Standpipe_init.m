function [param, outputs, options, init_done] = Standpipe_init()
% Returns standpipe parameters.

% structure for passing parameteres to function Standpipe_calc
param = struct('qIn_Max', 45.74, ...      % cm^3/s
              'hMax', 150, ...           % cm
              'A', 15.5, ...                 % cm^2
              'CdA', 0.045, ...          % cm^2
              'Cda', 0, ...                  % will be calculated, cm^2
              'g', 981, ...                  % cm/s^2
              'T1_Valve', 0.5, ...       % s
              'vIntercept', -0.25, ... % -
              'vSlope', 0.25, ...        % V^-1
              'sIntercept', 3, ...        % V
              'sSlope', 0.0467, ...    % V/cm
              'stdev_q', 0.5, ...           % cm^3/s
              'stdev_v_hi', 0.5, ...    % V
             'stdev_v_lo', 0.1);       % V

% Initial conditions:
outputs = [0, 0];        % [ qin(0), h(0)]

% Options for solver
options = odeset('NonNegative', [1, 2]);

init_done = 0; % dummy to force correct execution order
end%function