function outputs = Standpipe_calc(inputs, outputs, dt, param, options)

% Inputs
v_Valve = inputs(1);      % Valve voltage
normOut = inputs(2);      % Normal outflow valve open / closed
distOut = inputs(3);      % Disturbance outflow valve open / closed
much_noise = inputs(4);   % Much measurement noise due to splashing

% Steady state inflow rate
qIn_ss = max([0, (param.vIntercept + param.vSlope * v_Valve) * param.qIn_Max]);

% Noise of inflow rate
if qIn_ss == 0
    noise_q = 0;
else
    noise_q = param.stdev_q * randn(1);
end

% Cd*A for outflow
param.Cda = (normOut + distOut) * param.CdA;

% Initial conditions
qIn = outputs(1);
h = outputs(2);

% Solve DEs
[t,x] = ode45(@(t, x) Standpipe_DE(t, x, qIn_ss, noise_q, param), ...
           [0 dt/2 dt], [qIn, h], options);

% Determine Outputs
outputs(1) = x(3,1);         % outputs(1) = qIn
if x(3,2) >= param.hMax
    outputs(2) = param.hMax; % outputs(2) = h
    outputs(3) = 1;          % outputs(3) = overflow
else
    outputs(2) = x(3,2);
    outputs(3) = 0;
end
outputs(4) = param.sIntercept + param.sSlope*outputs(2) ...  % sensor voltage
    + (param.stdev_v_lo + much_noise*(param.stdev_v_hi - param.stdev_v_lo))*randn(1);
              

end


function dxdt = Standpipe_DE( t, x, qIn_ss, noise_q, param)
%
%Calculation of derivatives for standpipe simulation
%  
% x(1): Inflow rate (qIn)
% x(2): Level in standpipe (h)

qIn = x(1);
h = x(2);

dxdt = zeros(2,1);          % column vector

% Outflow rate
qOut = param.Cda * sqrt(2 * param.g * max([h, 0]));

% Desired inflow rate.
qIn_ss = max([qIn_ss + noise_q, 0]);

% Derivatives
dxdt(1) = (qIn_ss - qIn) / param.T1_Valve; % Valve dynamics.
dxdt(2) = (qIn - qOut)/param.A; % Column dynamics.

end

