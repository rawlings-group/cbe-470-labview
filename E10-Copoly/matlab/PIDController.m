function [u]=PIDController(e,Kc,tauI,tauD,dt)
% Written by Ankur Gupta, February 20, 2012
% This function implements a n x n PID controller matrix using a position
% form discrete time controller as described on page 961, Ogunnaike and
% Ray. Here, n=number of manipulated variables=number of measured
% variables.
% Inputs: 
% e=error matrix of size n x k (last k error vectors). e(:,k) is the latest
% error vector. k >=2
% Kc = Controller gains matrix n x n
% tauI = Intrgral times matrix n x n
% tauD = Derivative times matrix n x n
% dt = scalar time resolution for PID controller. This remains same for all
% n^2 controllers.
% Outputs:
% u = manipulated variables, n x 1

% Check sizes
[n,k]=size(e);
if k<2
    disp('Error: Incorrect size of e. size(e,2) has to be at least 2.');
    size(e)
end
[temp1,temp2]=size(Kc);
if temp1~=temp2 || temp1~=n
    disp('Error: Kc has wrong size.');
    size(Kc)
end
[temp1,temp2]=size(tauI);
if temp1~=temp2 || temp1~=n
    disp('Error: tauI has wrong size.');
    size(tauI)
end
[temp1,temp2]=size(tauD);
if temp1~=temp2 || temp1~=n
    disp('Error: tauD has wrong size.');
    size(tauD)
end
[temp1,temp2]=size(dt);
if temp1~=temp2 || temp1~=1
    disp('Error: dt is not scalar or has wrong size.');
    size(dt)
end

% Check data correctness
if(any(any(tauI)==0))
    disp('Error: tauI contains zeros entries.');
    tauI
end

sum_e=sum(e,2);
diff_e=e(:,k)-e(:,k-1);
u=Kc*e(:,k)+dt*(Kc./tauI)*sum_e+dt*(Kc.*tauD)*diff_e;






