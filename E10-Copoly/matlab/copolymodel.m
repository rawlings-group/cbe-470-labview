function [f]=copolymodel(t,y,p)
% This function provides the RHS of the ODEs for copolymerization reactor

M1=y(1);
M2=y(2);
I=y(3);
T=y(4);

% Compute rate constants
kd=p.kdhat*exp(-p.Ed/(p.Rgc*T));
kp11=p.kp11hat*exp(-p.Ep11/(p.Rgc*T));
kp12=p.kp12hat*exp(-p.Ep12/(p.Rgc*T));
kp21=p.kp21hat*exp(-p.Ep21/(p.Rgc*T));
kp22=p.kp22hat*exp(-p.Ep22/(p.Rgc*T));

extconv=(p.M1f+p.M2f-M1-M2)/(p.M1f+p.M2f+p.Sf);
arg11=min(-extconv*(p.b11+p.b12*T),0);
arg22=min(-extconv*(p.b21+p.b22*T),0);

% Gel factors
gt11=exp(arg11);
gt22=exp(arg22);
kt11=p.kt11hat*exp(-p.Et11/(p.Rgc*T))*gt11;
kt22=p.kt22hat*exp(-p.Et22/(p.Rgc*T))*gt22;
kt12=sqrt(kt11*kt22);

% Compute P and Q
Qnum=2*p.fi*kd*I-p.q*p.Bf/p.V;
if Qnum<0
    Qnum=0;
end
ratesratio=(kp21*M1)/(kp12*M2);
Qden=kt11*(ratesratio)^2+2*kt12*ratesratio+kt22;
if abs(Qden)<10^-5
    Qden=10^-5;
end
Q=sqrt(Qnum/Qden);
P=ratesratio*Q;

% Compute RHS of ODEs
f(1,1)=(p.M1f-M1)*p.q/p.V-M1*(kp11*P+kp21*Q);
f(2,1)=(p.M2f-M2)*p.q/p.V-M2*(kp12*P+kp22*Q);
f(3,1)=(p.If-I)*p.q/p.V-kd*I;
f(4,1)=(p.Tf-T)*p.q/p.V-(p.UA/(p.rhocp*p.V))*(p.Sw*(T-p.Tc)+(1-p.Sw)*(p.Th-T))-(p.dH0*kd*I+p.dHp11*kp11*M1*P+p.dHp21*kp21*M1*Q+p.dHp12*kp12*M2*P+p.dHp22*kp22*M2*Q)/p.rhocp;
f(4,1)=f(4,1)/(1+p.em);

%keyboard
