function [tfinish, I_f, UA, e] = controller(deltat, tstart, ...
    Tsp, Xsp, Kc, tauI, tauD, GI, T, X, e)
% Runs one step of control for the copoly process.

tfinish = tstart + deltat;
Tsp = Tsp + 273.15; % Temperatures to Kelvin.
T = T + 273.15;

% Max number of previous errors to store. Must be >=2
n_history = 10;

% PID Control
e_latest = [Tsp-T; Xsp-X];
e = [e e_latest];

if size(e,2) > n_history
    e = e(:,end-n_history+1:end);
end
v = PIDController(e,Kc,tauI,tauD,deltat);
u = GI*v; % Implement the decoupler
I_f = u(1);
UA = u(2);

end%function
