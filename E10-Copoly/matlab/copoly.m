function [M1,M2,I,T,P,X]=copoly(If,UA,R,Bf,M10,M20,I0,T0,tstart,tfinish)
% This file simulates the Copolymerization Reactor for Experiment 10, CBE 470 Lab
% Model is a mix of the source code (written on Oct 18, 1994) and the paper Hamer et al, 1981
% Written by Ankur Gupta

%close all; clear all; clc

p.If=If;
p.UA=UA;
p.R=R;
p.Bf=Bf;

% Input (manipulated) variables
%p.If=0.03; %moles/litres-sec (Initiator concentration in feed)
%p.UA=5.4; %kcal/sec-K (Heat Trasfer Coefficient)

% Disturbance variables
%p.R=0.0101; % M1f/M2f Monomer feed ratio
%p.R=0.02; % M1f/M2f Monomer feed ratio
%p.Bf=0.0; %moles/litres-sec (Inhibitor concentration in feed)

%%%%%%%%%%%
% Create a structure for parameters
p.f_act=1.147; %don't yet know what it is

% Rate constants and activation energies
p.kdhat=5.95*10^13; %sec^-1
p.Ed=29.6; %kcal/mole

p.kp11hat=7*10^6; %litres/(moles-sec)
p.Ep11=6.3; %kcal/mole
p.r1=22;
p.kp12hat=p.kp11hat/p.r1;
p.Ep12=p.Ep11;

p.kp22hat=1.3*10^8; %litres/(moles-sec)
p.Ep22=p.Ep11;
p.r2=0.04;
p.kp21hat=p.kp22hat/p.r2;
p.Ep21=p.Ep11;

p.kt11hat=1.76*10^9; %litres/(moles-sec)
p.Et11=2.8; %kcal/mole

p.kt22hat=2.8*10^10; %litres/(moles-sec)
p.Et22=2.8; %kcal/mole

% Temperature-related constants
p.dH0=-30; %kcal/moles (Heat of reaction I --> 2R)
p.dHp11=-13.5; %kcal/moles
p.dHp12=-13.5; %kcal/moles
p.dHp21=-21.0; %kcal/moles
p.dHp22=-21.0; %kcal/moles

p.Tc=313; %K (cooling source temperature)
p.Th=373; %K (heating source temperature)
p.rhocp=0.4; %kcal/litres-K

% don't know what these are
p.b11=54.5;
p.b12=-0.13;
p.b21=32;
p.b22=-0.08;

% Flow and volume constants
p.V=10000; %litres (Reactor volume)
p.q=1.5; %litres/sec (inlet and outlet flowrate)

% Feed conditions
p.M1fs=0.0728; %moles/litres-sec
p.M2fs=7.2072; %moles/litres-sec
p.Sf=3.12; %moles/litres-sec
p.Tf=303; %K (inlet flow temperature)
p.M2f=(p.M2fs+p.f_act*p.M1fs)/(1+p.R*p.f_act); %don't know why this is done
p.M1f=p.R*p.M2f; % don't know why this is done either

% Other constants
p.em=0.8; % don't know what it is
p.fi=0.6; % initiator efficiency
%p.Rgc=1.9858775/1000; %kcal/mol-K (Universal Gas Constant)
p.Rgc=1.987/1000;
if (p.UA<0)
    % We're heating the CSTR
    p.Sw=0;
else
    % We're cooling the CSTR
    p.Sw=1;
end

% Time points at which we need to obtain the solution
%tstart=0; %seconds
%tfinish=2; %seconds
%deltat=0.1; %seconds
%deltat=2; %seconds
t=[tstart tfinish];

% Initial condition
%y0=[3.4598e-4; 0.76772; 0.0277743; 346.069; 0];
y0=[M10 M20 I0 T0]';
y0(5)=exp(10.9351-3825.666/y0(4));

% Check for a steady stste
%y0new=fsolve(@(y) copolymodel(0,y,p), y0,optimset('Display','iter'));

% % Solve the ode till tfinish
% [tout,yout]=ode23s(@(t,y) copolymodel(t,y,p),tspan,y0);
% yout(:,5)=exp(10.9351-3825.666./yout(:,4));

y=zeros(5,length(t));
y(:,1)=y0;

if tstart==tfinish
    % Don't use ode23s; it will fail
    y(:,2)=y(:,1);
else
    for i=1:length(t)-1
        y(:,i+1)=copolysample(y(:,i),t(i),t(i+1),p);
        
        %     figure(1);
        %     title(['Time=' num2str(t(i+1)) ' secs']);
        %     subplot(2,2,1);
        %     plot(t(1:i+1),y(1,1:i+1),'r-',t(1:i+1),y(2,1:i+1),'b-');
        %     ylabel('M_1, M_2 (moles/litres)');
        %     xlabel('time (secs)');
        %     legend('M_1','M_2');
        %     grid on
        %
        %     subplot(2,2,2);
        %     plot(t(1:i+1),y(3,1:i+1),'b-');
        %     ylabel('I (moles/litres)');
        %     xlabel('time (secs)');
        %     legend('I');
        %     grid on
        %
        %     subplot(2,2,3);
        %     plot(t(1:i+1),y(4,1:i+1),'b-');
        %     ylabel('Temperature (K)');
        %     xlabel('time (secs)');
        %     legend('T');
        %     grid on
        %
        %     subplot(2,2,4);
        %     plot(t(1:i+1),y(5,1:i+1),'b-');
        %     ylabel('Pressure (atm)');
        %     xlabel('time (secs)');
        %     legend('P');
        %     grid on
        %     pause(0.02);
    end
end

% Check for negative or out of bound values
% find(y<=0)
% find(y(:,1)>p.M1f)
% find(y(:,2)>p.M2f)
% find(y(:,3)>1)
% find(y(:,4)<p.Tf)

M1=y(1,end);
M2=y(2,end);
I=y(3,end);
T=y(4,end);
P=y(5,end);
%X=(p.M1f+p.M2f-M1-M2)/(p.M1f+p.M2f+p.Sf); % Conversion
X=(p.M1f+p.M2f-M1-M2)/(p.M1f+p.M2f); % Conversion. Changed on Mar 21, 2012 to match the lab manual and my notes

% M1=2;
% M2=3;
% I=10;
% T=400;
% P=1;

if isnan(M1)==1
    save data_nan.mat
end



