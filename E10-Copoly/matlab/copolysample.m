function ynext=copolysample(y,t,tnext,p)
% This function solves the ODEs at the next sampling time point

% Solve the ode till tfinish
[tout, yout] = ode23s(@(t,y) copolymodel(t,y,p), [t tnext], y(1:4));
yout(:,5) = exp(10.9351-3825.666./yout(:,4)); % Calculate reaction rate.

if tout(end) ~= tnext
  disp('Error in copolysample');
end
ynext = yout(end,:)';
end%function
